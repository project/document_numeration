<?php

namespace Drupal\document_numeration\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'number_per_year' field type.
 *
 * @FieldType(
 *   id = "number_per_year",
 *   label = @Translation("Number per year"),
 *   category = @Translation("Custom"),
 *   description = @Translation("Number Per Year"),
 *   default_widget = "number_per_year",
 *   default_formatter = "number_per_year"
 * )
 */
class NumberPerYearField extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'initial_number' => 1,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Prevent early t() calls by using the TranslatableMarkup.
    $properties['year'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Year'))
      ->setRequired(TRUE);

    $properties['digit'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Digit'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'year' => [
          'type' => 'int',
          'not null' => TRUE,
        ],
        'digit' => [
          'type' => 'int',
          'not null' => TRUE,
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = rand(1, 10000);
    $random_formatted = str_repeat('0', $field_definition->getSetting('digit_numbers') - strlen($random)) . $random;
    $values['year'] = date('Y');
    $values['digit'] = $random_formatted;

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $elements = [];

    $elements['initial_number'] = [
      '#type' => 'number',
      '#title' => t('Initial number'),
      '#default_value' => $this->getSetting('initial_number'),
      '#required' => TRUE,
      '#description' => t('The initial number for the current yar.'),
      '#min' => 1,
      '#disabled' => $has_data,
    ];

    $elements['document_numeration'] = [
      '#type' => 'value',
      '#value' => 'document_numeration',
    ];

    return $elements;
  }

  /**
   * Validate the settings form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateForm(array $form, FormStateInterface $form_state) {
    $cardinality = $form_state->getValue('cardinality');
    $cardinality_number = $form_state->getValue('cardinality_number');

    if ($cardinality !== 1) {
      $form_state->setErrorByName('cardinality', t('This field only allows one value.'));
    }

    if ($cardinality_number !== 1) {
      $form_state->setErrorByName('cardinality_number', t('This field only allows one value.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('digit')->getValue();

    return $value === NULL || $value === '';
  }

}

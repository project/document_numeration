<?php

namespace Drupal\document_numeration\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'number_per_year' formatter.
 *
 * @FieldFormatter(
 *   id = "number_per_year",
 *   label = @Translation("Number per year"),
 *   field_types = {
 *     "number_per_year"
 *   }
 * )
 */
class NumberPerYearFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'format' => 'yyyy-n',
      'digit_numbers' => 4,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['digit_numbers'] = [
      '#type' => 'number',
      '#title' => t('Minimum nNumbers of digits'),
      '#default_value' => $this->getSetting('digit_numbers'),
      '#required' => TRUE,
      '#description' => t('The minimum length of digits in the numbers (n) on nn-yyyy.'),
      '#min' => 1,
    ];

    $form['format'] = [
      '#type' => 'select',
      '#title' => t('Format'),
      '#default_value' => $this->getSetting('format'),
      '#options' => [
        'yyyy-n' => 'yyyy-n',
        'n-yyyy' => 'n-yyyy',
      ],
      '#required' => TRUE,
      '#description' => t('The numeration format.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = t('Number of digits: @number', ['@number' => $this->getSetting('digit_numbers')]);
    $summary[] = t('Format: @format', ['@format' => $this->getSetting('format')]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {

      if (($this->getSetting('digit_numbers') - strlen($item->digit) > 0)) {
        $digit = str_repeat('0', $this->getSetting('digit_numbers') - strlen($item->digit)) . $item->digit;
      }
      else {
        $digit = $item->digit;
      }

      $year = $item->year;
      $value = $item->format == 'yyyy-n' ? "$year-$digit" : "$digit-$year";

      $number_per_year = [
        '#theme' => 'document_numeration',
        '#document_number' => $value,
      ];

      $elements[$delta] = ['#markup' => \Drupal::service('renderer')->render($number_per_year)];
    }

    return $elements;
  }

}

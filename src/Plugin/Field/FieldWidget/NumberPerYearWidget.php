<?php

namespace Drupal\document_numeration\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'number_per_year' widget.
 *
 * @FieldWidget(
 *   id = "number_per_year",
 *   label = @Translation("Number per year"),
 *   field_types = {
 *     "number_per_year"
 *   }
 * )
 */
class NumberPerYearWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'size' => 60,
      'digit_numbers' => 4,
      'initial_number' => 1,
      'format' => 'yyyy-n',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    if (isset($items[$delta]->year) && isset($items[$delta]->digit)) {
      $digit = str_repeat('0', $this->fieldDefinition->getSetting('digit_numbers') - strlen($items[$delta]->digit)) . $items[$delta]->digit;
      $year = $items[$delta]->year;
      $is_new = FALSE;
    }
    else {
      $digit = str_repeat('0', $this->getSetting('digit_numbers'));
      $year = date('Y');
      $is_new = TRUE;
    }

    $value = $this->getFieldSetting('format') == 'yyyy-n' ? "$year-$digit" : "$digit-$year";
    if ($is_new) {
      $value .= ' * ' . t("It's is an example. The real numeration will be generated after you save this item.");
    }

    $element['document_number'] = [
      '#type' => '#markup',
      '#markup' => $value,
    ];

    return $element;
  }

}
